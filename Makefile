ifeq ($(shell hostname), pradeep-laptop)
	RUSTC = /usr/local/bin/rustc
else
	RUSTC = /home/spradeep/Applications/rust/bin/bin/rustc
endif

RUST_FLAGS = --cfg debug
SOURCE = my_pp.rs
# TODO: Call this rustfmt
TARGET = my_pp
TEST = test-pp

PP = my_pp
PP_LIB = libpp.dummy

DEEP_VISIT = deep_visit
DEEP_VISIT_LIB = libdeep_visit.dummy

.PHONY: all test clean

all: $(TARGET)

$(PP): $(SOURCE) $(PP_LIB) $(DEEP_VISIT_LIB)
	$(RUSTC) $(RUST_FLAGS) $< -L . -o $@

$(PP_LIB): pp.rs
	$(RUSTC) $(RUST_FLAGS) --lib $^
	@touch $@

$(DEEP_VISIT_LIB): deep_visit.rs
	$(RUSTC) $(RUST_FLAGS) --lib $^
	@touch $@

# --------------------

# This was for TARGET = pprust
# $(TARGET): $(SOURCES) lib*.so
# 	$(RUSTC) $(RUST_FLAGS) $< -L . -o $@

# # FIXME: Hack to make sure pp.rs is not rebuilt every time
# libpp%.so: pp.rs
# 	$(RUSTC) $(RUST_FLAGS) --lib $^

# --------------------

$(TEST): $(SOURCE) $(PP_LIB)
	$(RUSTC) $(RUST_FLAGS) --test $< -L . -o $@

test: $(TEST)
	RUST_LOG=$(TEST),pp ./$(TEST)

integration_test: $(TARGET)
	@echo "Integration tests"
	@./run_and_comp.bash

clean:
	rm -rf *.so $(TEST) $(TARGET)
