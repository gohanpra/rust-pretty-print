Pretty-printer for Rust code
============================

# Usage

```
./pprust foo.rs
```

This will print the "pretty" output to stdout.

# Setup

Right now, you have to compile pp.rs manually the first time, cos I figured out how to compile a Rust library using `make` without it being compiled every time.

(The main problem is that you don't know the name of the output library file in advance.)

## To build my_pp.rs

```
make my_pp
```

# Warning

The code here has been adopted from the pretty printer code in the Rust source (libsyntax/print). The pretty-printer there is not being maintained actively and is just plain wrong for the latest Rust code.

I have done the *minimum* necessary to get it to work in isolation.

I have not tried to reproduce the testing infrastructure from compiletest. That stuff seemed to be depend a lot on the rest of the rustc code.

There is a lot of cruft from the original code - lots of managed pointers, etc. Plus, the original code ugly-prints a lot of stuff.

I plan to fix all that incrementally.

# Plan

* Fix the major pretty-printing issues.
* Document the pprust code.
* Maybe change the code to implement the `Visitor` trait for better extensibility and consistency
  (instead of writing arbitrary print_* functions).
* Remove dependence on the libsyntax/print/pp.rs.
  That stuff implements the consistent vs inconsistent line-breaking logic.
* Remove the managed pointers.
* Import the rest of the pretty-print tests from rustc.
* Implement the pretty-print test directives (`pp-exact`, etc.)
* Lastly, make it efficient (cos I want to write a quick first version that works right without caring about efficiency.)

# Credits

I've based my code off the original pp.rs and pprust.rs from the Rust source.
So, the credit for the base code goes to the contributors who wrote them.
