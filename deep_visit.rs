#[feature(managed_boxes)];
#[feature(globs)];

#[link(name = "deep_visit", vers = "0.1")];

extern mod syntax;

use syntax::visit::*;
use syntax::ast;
use syntax::ast::*;
use syntax::codemap::Span;
use syntax::opt_vec::OptVec;

/// Visitor for the parts of the AST that don't have a visit_* function.
///
/// The original visit.rs uses walk_* functions for these.
/// I'm doing this mainly for uniformity and ease of reasoning.
trait DeepVisitor<E:Clone>: Visitor<E> {
    fn visit_enum_def(&mut self,
                      enum_definition: &ast::enum_def,
                      generics: &Generics,
                      env: E) {
        walk_enum_def(self, enum_definition, generics, env);
    }

    fn visit_expr_opt(&mut self,
                      optional_expression: Option<@Expr>,
                      env: E) {
        walk_expr_opt(self, optional_expression, env);
    }

    fn visit_exprs(&mut self,
                   expressions: &[@Expr],
                   env: E) {
        walk_exprs(self, expressions, env);
    }

    // fn foo() or extern "Abi" fn foo()
    fn visit_fk_item_fn_decl(&mut self,
                             function_declaration: &fn_decl,
                             env: E) {
        walk_fn_decl(self, function_declaration, env);
    }

    // |x, y| -> int { ... }
    fn visit_fk_fn_block(&mut self,
                         function_declaration: &fn_decl,
                         body: &Block,
                         is_body_of_do: bool,
                         s: Span,
                         n: NodeId,
                         env: E) {}

    // proc(x, y) -> int { ... }
    fn visit_fk_fn_block_proc(&mut self,
                              fd: &fn_decl,
                              b: &Block,
                              s: Span,
                              n: NodeId,
                              env: E) {}
    // |x, y|
    fn visit_fk_fn_block_decl(&mut self,
                              function_declaration: &fn_decl,
                              env: E) {
        walk_fn_decl(self, function_declaration, env);
    }

    // proc()
    fn visit_fk_fn_block_proc_decl(&mut self,
                                   function_declaration: &fn_decl,
                                   env: E) {}

    fn visit_method_helper(&mut self,
                           method: &method,
                           env: E) {
        walk_method_helper(self, method, env);
    }

    fn visit_if(&mut self, test: @Expr, blk: &Block, elseopt: Option<@Expr>, env: E) {
        self.visit_expr(test, env.clone());
        self.visit_block(blk, env.clone());
        walk_expr_opt(self, elseopt, env.clone());
    }

    fn visit_path(&mut self, path: &Path, env: E) {
        walk_path(self, path, env);
    }

    fn visit_trait_ref(&mut self,
                       trait_ref: &ast::trait_ref,
                       env: E) {
        // Copied from visit.rs
        walk_path(self, &trait_ref.path, env);
    }

    fn visit_ty_param_bounds(&mut self,
                             bounds: &OptVec<TyParamBound>,
                             env: E) {
        walk_ty_param_bounds(self, bounds, env);
    }

    fn visit_literal(&mut self, lit: &ast::lit, env: E) {}

    fn visit_arg(&mut self, arg: &ast::arg, env: E) {}

    fn visit_field(&mut self, field: &ast::Field, env: E) {}

    fn visit_ident(&mut self, ident: &ast::Ident, env: E) {}

    fn visit_expr_vstore(&mut self, t: &ast::ExprVstore, env: E) {}
}
