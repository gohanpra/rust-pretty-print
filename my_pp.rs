#[feature(managed_boxes)];
#[feature(globs)];

extern mod syntax;
extern mod extra;
extern mod pp(name = "pp", vers = "0.1");
extern mod deep_visit(name="deep_visit", vers = "0.1");

use std::path::Path;
use std::io::fs::File;
use std::io::mem::MemWriter;
use std::io::Decorator;
use std::io;
use std::str;
use extra::getopts::*;
use std::os;
use syntax::ast::*;
use syntax::ast_util;
use syntax::visit::*;
use syntax::codemap::Span;
use syntax::parse::token::{ident_to_str};
use syntax::parse::classify::expr_is_simple_block;
use syntax::parse;
use string_parser::*;
use std::rc::RcMut;

use deep_visit::{DeepVisitor};

use pp::{cbox, ibox, word, end, eof, break_offset, hardbreak};

// This includes the contents of string_parser.rs as a module
// `string_parser` in this crate.
mod string_parser;

static DEFAULT_WIDTH: uint = 78u;
static INDENT_UNIT: int = 4;

/// This will collect the pretty-printed string as we traverse the AST.
/// This will be shallow-copied when you pass it to a function or assign to a
/// variable.
#[deriving(Clone)]
struct PpEnv {
    printer: @mut pp::Printer,
    output: @mut MemWriter,
}

// impl<S: Str> Equiv<S> for PpEnv {
//     fn equiv(&self, other: &S) -> bool {
//         do self.pp_str.with_borrow |val| {
//             println!("self: {:?}", val);
//             println!("other: {:?}", other);
//             val.equiv(other)
//         }
//     }
// }

impl ToStr for PpEnv {
    fn to_str(&self) -> ~str {
        // Doesn't work
        let wr = self.output;
        str::from_utf8(*wr.inner_ref());
        ~""
    }
}

impl PpEnv {
    fn new() -> PpEnv {
        let writer = @mut MemWriter::new();
        PpEnv {
            printer: pp::mk_printer(writer as @mut io::Writer, DEFAULT_WIDTH),
            output: writer,
        }
    }

    fn nbsp(&self) {
        word(self.printer, " ");
    }

    fn popen(&self) {
        word(self.printer, "(");
    }

    fn pclose(&self) {
        word(self.printer, ")");
    }
}

/// The main invariant is that each visit_ function should produce tokens for
/// that particular part of the AST.
/// e.g., a visit_fn should produce tokens for "fn foo() {}", etc.
/// Right now, we don't assume that each visit_ function produces a complete
/// PP block or something.
///
/// TODO: How to enforce that using the Type system?
/// We can make sure that the only way to add something to PpEnv is to send it
/// a full Foo where Foo is the thing that every visit_ should produce.
struct PpVisitor;

/// Visitor
///
/// Note: Square brackets [] represent BEGIN and END pretty-printing tokens in
/// the comments.
impl Visitor<PpEnv> for PpVisitor {
    fn visit_fn(&mut self, fk: &fn_kind, fd: &fn_decl, b: &Block, s: Span, n: NodeId, env: PpEnv) {
        // println("visit_fn:");
        match *fk {
            // [ [decl {] [ body ] [}] ]
            fk_item_fn(name, generics, purity, _) => {
                cbox(env.printer, 0u);

                // TODO: Find out later what the right behaviour is when the
                // declaration is very long.
                ibox(env.printer, 0u);
                word(env.printer, "fn");
                env.nbsp();
                word(env.printer, ident_to_str(&name));

                self.visit_fk_item_fn_decl(fd, env);

                break_offset(env.printer, 1u, 0);

                // This will close the ibox
                self.visit_block(b, env);

                end(env.printer);
            }
            fk_fn_block => {
                self.visit_fk_fn_block(fd, b, false, s, n, env);
            }
            _ => {}
        }
    }

    /// We have to handle three cases:
    /// empty block: "{}"
    /// one-line block: "{" SPACE statements SPACE "}"
    /// multi-line block: "{" BREAK(4) [STMT_1 BLANK STMT_2 ... BLANK STMT_N] BREAK "}"
    ///
    /// NOTE: The only problem with this is that it now becomes procedural. It
    /// depends on who's calling and in what kind of condition.
    ///
    /// ASSUMPTION: This is called within an open box. That box will be closed
    /// after the opening brace.
    fn visit_block(&mut self, block: &Block, env: PpEnv) {

        // TODO: Maybe add an assert for the Assumption

        word(env.printer, "{");
        end(env.printer);

        // Empty block
        if block.view_items.is_empty() && block.stmts.is_empty() && block.expr.is_none() {
            // TODO: This is only there to handle a case where the fn_decl
            // extends up to the last char of the line and the {} comes on the
            // next line because there is no break between them. (see
            // fn_long_fn_name_brace_on_same_line.pp)
            // TODO: But this puts an extra newline between the braces when
            // the opening brace comes on a new line.
            // break_offset(env.printer, 0u, 0);

            word(env.printer, "}");
            return;
        }

        break_offset(env.printer, 1u, INDENT_UNIT);

        // TODO: Do we really need this box?
        // See if people prefer
        // fn long_name {
        //     let x = foo(); x
        // }
        // over
        // fn long_name {
        //     let x = foo();
        //     x
        // }
        cbox(env.printer, 0);

        for view_item in block.view_items.iter() {
            self.visit_view_item(view_item, env.clone());
        }

        if !block.view_items.is_empty() && (!block.stmts.is_empty() || block.expr.is_some()) {
            break_offset(env.printer, 1u, 0);
        }

        join_fns_with_sep(block.stmts,
                          |stmt| self.visit_stmt(*stmt, env.clone()),
                          // Using hardbreak cos I've never seen multiple
                          // statements on the same line anywhere.
                          || hardbreak(env.printer));

        if (!block.view_items.is_empty() || !block.stmts.is_empty()) && block.expr.is_some() {
            break_offset(env.printer, 1u, 0);
        }

        match block.expr {
            Some(expression) => {
                self.visit_expr(expression, env);
            }
            None => {}
        }

        end(env.printer);
        break_offset(env.printer, 1u, 0);
        word(env.printer, "}");
    }

    fn visit_stmt(&mut self, st: @Stmt, env: PpEnv) {
        // maybe_print_comment(s, st.span.lo);
        match st.node {
            StmtDecl(decl, _) => {
                self.visit_decl(decl, env);
            }
            StmtExpr(expr, _) => {
                debug!("StmtExpr");
                self.visit_expr(expr, env);
            }
            StmtSemi(expr, _) => {
                self.visit_expr(expr, env);
                word(env.printer, ";");
            }
            StmtMac(ref mac, semi) => {
                // space_if_not_bol(s);
                // print_mac(s, mac);
                // if semi { word(env.printer, ";"); }
            }
        }
        // if parse::classify::stmt_ends_with_semi(st) { word(env.printer, ";"); }
        // maybe_print_trailing_comment(s, st.span, None);
    }

    // TODO: Should each expression have a box of its own.
    // Example where this is necessary?
    // Example where this is bad? let x = Foo { ... }; where we want the
    // closing brace to come in the first column
    fn visit_expr(&mut self, expr: @Expr, env: PpEnv) {
        match expr.node {
            ExprVstore(e, v) => {
                self.visit_expr_vstore(&v, env);
                cbox(env.printer, 0);
                self.visit_expr(e, env);
                end(env.printer);
            },
            ExprVec(ref exprs, mutbl) => {
                // TODO: Should this be inside the box or outside?
                word(env.printer, "[");
                cbox(env.printer, 0);

                // TODO: What is this for?
                // if mutbl == MutMutable {
                //     word(env.printer, "mut");
                //     if exprs.len() > 0u { env.nbsp(); }
                // }

                join_fns_with_sep(
                    *exprs,
                    |e| self.visit_expr(*e, env),
                    || {
                        word(env.printer, ",");
                        break_offset(env.printer, 1u, 0);
                    });
                end(env.printer);
                word(env.printer, "]");
            }
            ExprRepeat(element, count, mutbl) => {
                word(env.printer, "[");
                if mutbl == MutMutable {
                    word(env.printer, "mut");
                    env.nbsp();
                }
                cbox(env.printer, 0);
                self.visit_expr(element, env);
                word(env.printer, ",");
                env.nbsp();
                word(env.printer, "..");
                self.visit_expr(count, env);
                word(env.printer, "]");
                end(env.printer);
            }

            // ... = Foo { x: Bar, y: Baz, } or
            // ... = Foo {
            //     x: Bar,
            //     y: Baz,
            // }
            // Assumption: We are inside a cbox(0)
            ExprStruct(ref path, ref fields, wth) => {
                // cbox(env.printer, 0);
                self.visit_path(path, env);
                env.nbsp();
                word(env.printer, "{");
                break_offset(env.printer, 1u, INDENT_UNIT);

                // cbox(env.printer, 0);
                join_fns_with_sep(
                    *fields,
                    |f| self.visit_field(f, env),
                    || {
                        word(env.printer, ",");
                        break_offset(env.printer, 1u, INDENT_UNIT);
                    });
                // commasep_cmnt(env.printer, consistent, (*fields), print_field, get_span);
                // end(env.printer);

                // TODO:
                match wth {
                    Some(expr) => {
                        ibox(env.printer, INDENT_UNIT as uint);
                        word(env.printer, ",");
                        break_offset(env.printer, 1u, 0);
                        word(env.printer, "..");
                        self.visit_expr(expr, env);
                        end(env.printer);
                    }
                    _ => (word(env.printer, ","))
                }
                break_offset(env.printer, 1u, 0);
                word(env.printer, "}");
                // end(env.printer);
            }
            // (x, y, z) or
            // (x,
            //  y,
            //  z)
            ExprTup(ref exprs) => {
                env.popen();
                // TODO: Maybe have a zero-break over here so that all the
                // expressions start from BOL ???

                cbox(env.printer, 0);
                join_fns_with_sep(
                    *exprs,
                    |e| self.visit_expr(*e, env),
                    || {
                        word(env.printer, ",");
                        break_offset(env.printer, 1u, 0);
                    });
                end(env.printer);

                // commasep_exprs(env.printer, inconsistent, *exprs);

                if exprs.len() == 1 {
                    word(env.printer, ",");
                }
                env.pclose();
            }
            ExprCall(func, ref args, sugar) => {
                match sugar {
                    // "foo (" [arg1, arg2, arg3] ")"
                    // TODO: For now, I'm going with no break after ( and before )
                    NoSugar => {
                        self.visit_expr(func, env);
                        env.popen();

                        // TODO: Refactor into a function
                        // TODO: Maybe have a zero-break over here so that all the
                        // expressions start from BOL ???
                        cbox(env.printer, 0);
                        join_with_comma(args.as_slice(),
                                        |e| self.visit_expr(*e, env),
                                        env);
                        end(env.printer);
                        env.pclose();
                    }
                    // do func([arg1, arg2]) " " fn_block
                    // Note: If fn_block has no parameters, omit the ||s
                    DoSugar => {
                        word(env.printer, "do");
                        env.nbsp();
                        self.visit_expr(func, env);
                        if args.len() > 1 {
                            env.popen();
                            cbox(env.printer, 0);
                            join_with_comma(
                                args.slice_to(args.len() - 1),
                                |e| self.visit_expr(*e, env),
                                env);
                            end(env.printer);
                            env.pclose();
                        }

                        env.nbsp();

                        let blk = args[args.len() - 1];
                        match blk.node {
                            ExprDoBody(body) => {
                                match body.node {
                                    ExprFnBlock(ref decl, ref body) => {
                                        self.visit_fk_fn_block(decl, body, true, expr.span,
                                                               expr.id,
                                                               env);
                                    }
                                    _ => { fail!(); }
                                }
                            }
                            _ => { fail!(); }
                        }
                    }
                    _ => {}
                }
            }
            ExprDoBody(body) => {
                self.visit_expr(body, env);
            }
            ExprFnBlock(ref decl, ref body) => {
                self.visit_fk_fn_block(decl,
                                       body,
                                       false,
                                       expr.span,
                                       expr.id,
                                       env)
            }
            ExprProc(ref decl, ref body) => {
                self.visit_fk_fn_block_proc(decl,
                                            body,
                                            expr.span,
                                            expr.id,
                                            env)
            }
            ExprBlock(ref blk) => {
                // For the block to close
                cbox(env.printer, 0);
                self.visit_block(blk, env);
            }
            ExprLit(lit) => {
                self.visit_literal(lit, env)
            }
            ExprIf(test, ref blk, elseopt) => {
                self.visit_if(test, blk, elseopt, env);
            }
            // [while expr {] block }
            ExprWhile(test, ref blk) => {
                // For the block to close
                cbox(env.printer, 0);
                word(env.printer, "while");
                env.nbsp();
                self.visit_expr(test, env);
                break_offset(env.printer, 1u, 0);
                self.visit_block(blk, env);
            }
            // [for pat in expr {] block }
            ExprForLoop(pat, iter, ref blk, opt_ident) => {
                // TODO:
                // for ident in opt_ident.iter() {
                //     word(env.printer, "'");
                //     self.visit_ident(*ident, env);
                //     word_space(s, ":");
                // }

                // For the block to close
                cbox(env.printer, 0);
                word(env.printer, "for");
                env.nbsp();
                self.visit_pat(pat, env);
                break_offset(env.printer, 1u, 0);
                word(env.printer, "in");
                break_offset(env.printer, 1u, 0);
                self.visit_expr(iter, env);
                break_offset(env.printer, 1u, 0);
                self.visit_block(blk, env);
            }
            ExprLoop(ref blk, opt_ident) => {
                // TODO:
                // for ident in opt_ident.iter() {
                //     word(env.printer, "'");
                //     self.visit_ident(*ident, env);
                //     word_space(s, ":");
                // }

                // For the block to close
                cbox(env.printer, 0);
                word(env.printer, "loop");
                env.nbsp();
                self.visit_block(blk, env);
            }
            ExprMatch(expr, ref arms) => {
                // [match expression {] BLANK(4) arms BLANK(0) }
                // arms: arm_1 BLANK(4) arm_2 BLANK(4) ... arm_N
                cbox(env.printer, 0);
                word(env.printer, "match");
                env.nbsp();
                self.visit_expr(expr, env);
                env.nbsp();
                word(env.printer, "{");
                end(env.printer);

                break_offset(env.printer, 1u, 4);

                // Put a comma at the end of the arm only if it has a simple
                // expression body.
                let mut is_arm_simple_expr = false;
                join_fns_with_sep(
                    *arms,
                    |arm| {
                        is_arm_simple_expr = is_simple_expr_body(&arm.body);
                        self.visit_arm(arm, env)
                    },
                    || {
                        if is_arm_simple_expr {
                            word(env.printer, ",");
                        }
                        break_offset(env.printer, 1u, 4);
                    });

                break_offset(env.printer, 1u, 0);

                word(env.printer, "}");
            }
            ExprPath(ref path) => self.visit_path(path, env),
            _ => {}
        }
    }

    /// [pats optional_guard => block]
    /// [pats optional_guard => expr]
    ///
    /// pats: pat_1 "|" pat_2 "|" ... pat_N
    ///
    /// multi-line pat:
    /// pat_1
    /// | pat_2
    /// | pat_3
    fn visit_arm(&mut self, arm: &Arm, env: PpEnv) {
        cbox(env.printer, 0);

        // For the block or expr to close
        cbox(env.printer, 0);

        join_fns_with_sep(
            arm.pats,
            |pat| self.visit_pat(*pat, env),
            || {
                break_offset(env.printer, 1u, 0);
                word(env.printer, "|");
                env.nbsp();
            });

        env.nbsp();

        // TODO: optional guard

        word(env.printer, "=>");

        break_offset(env.printer, 1u, 4);

        // Extract the expression from the extra block the parser adds
        // in the case of foo => expr
        if arm.body.view_items.is_empty() &&
            arm.body.stmts.is_empty() &&
            arm.body.rules == DefaultBlock &&
            arm.body.expr.is_some() {
            match arm.body.expr {
                Some(expr) => {
                    match expr.node {
                        ExprBlock(ref blk) => {
                            // Print the inner block and not arm.body cos
                            // otherwise you get nested empty braces.
                            self.visit_block(blk, env);
                        }
                        _ => {
                            // Close the box
                            end(env.printer);
                            self.visit_expr(expr, env);
                        }
                    }
                }
                None => {
                    fail!()
                }
            }
        } else {
            self.visit_block(&arm.body, env)
        }
        end(env.printer);
    }

    fn visit_decl(&mut self, decl: @Decl, env: PpEnv) {
        // maybe_print_comment(s, decl.span.lo);
        match decl.node {
            DeclLocal(ref loc) => self.visit_local(*loc, env),
            DeclItem(item) => {
                // print_item(s, item)
            }
        }
    }

    /// "let foo =" SPACE "expr;" or
    /// "let foo;"
    fn visit_local(&mut self, local: @Local, env: PpEnv) {
        cbox(env.printer, 0);

        word(env.printer, "let");
        env.nbsp();
        self.visit_pat(local.pat, env);

        match local.init {
            Some(init) => {
                env.nbsp();
                word(env.printer, "=");
                // TODO: Using a space here instead of break_offset so that
                // ExprStruct starts on the same line instead of breaking
                // arbitrarily
                env.nbsp();
                self.visit_expr(init, env);
            }
            _ => ()
        }
        word(env.printer, ";");

        end(env.printer);
    }

    fn visit_pat(&mut self, p: @Pat, env: PpEnv) {
        match p.node {
            PatIdent(_, ref path, ref optional_subpattern) => {
                self.visit_path(path, env);
                // TODO:
                // match *optional_subpattern {
                //     None => {}
                //     Some(subpattern) => self.visit_pat(subpattern, env),
                // }
            }
            PatEnum(ref path, ref args_) => {
                self.visit_path(path, env);
                match *args_ {
                    None => word(env.printer, "(*)"),
                    Some(ref args) => {
                        if !args.is_empty() {
                            word(env.printer, "(");
                            join_with_comma(
                                *args,
                                |&p| self.visit_pat(p, env),
                                env);
                            word(env.printer, ")");
                        }
                    }
                }
            }
            _ => {}
        }
    }

    fn visit_ty(&mut self, typ: &Ty, env: PpEnv) {
        match typ.node {
            ty_path(ref path, ref bounds, _) => {
                self.visit_path(path, env);
            }
            _ => {}
        }
    }
}

impl DeepVisitor<PpEnv> for PpVisitor {
    /// [|x, y| -> foo {] body [}]
    /// [|x, y| -> foo] expr
    ///
    /// If is_body_of_do, then if there are no parameters and no return type,
    /// omit the ||s
    fn visit_fk_fn_block(&mut self,
                         fd: &fn_decl,
                         b: &Block,
                         is_body_of_do: bool,
                         s: Span,
                         n: NodeId,
                         env: PpEnv) {
        ibox(env.printer, 0u);

        let has_empty_fn_decl = match fd.output.node {
            ty_infer if fd.inputs.is_empty() => true,
            _ => false,
        };
        if !(is_body_of_do && has_empty_fn_decl) {
            self.visit_fk_fn_block_decl(fd, env);
            break_offset(env.printer, 1u, 4);
        }

        // NOTE: In the AST implementation, the body of the function
        // block is stored in the "expr" field of the Block.
        assert!(b.stmts.is_empty());
        assert!(b.expr.is_some());

        // We extract the block, so as not to create another set of boxes
        match b.expr.unwrap().node {
            ExprBlock(ref blk) => {
                // This will close the ibox
                self.visit_block(blk, env);
            }
            _ => {
                // Close the ibox.
                end(env.printer);
                // This is a bare expression.
                // e.g., |x| foo(x)
                self.visit_expr(b.expr.unwrap(), env);
            }
        }
    }

    /// [proc(x, y) -> foo {] body [}]
    /// [proc(x, y) -> foo] expr
    fn visit_fk_fn_block_proc(&mut self,
                              fd: &fn_decl,
                              b: &Block,
                              s: Span,
                              n: NodeId,
                              env: PpEnv) {
        ibox(env.printer, 0u);

        let has_empty_fn_decl = match fd.output.node {
            ty_infer if fd.inputs.is_empty() => true,
            _ => false,
        };

        self.visit_fk_fn_block_proc_decl(fd, env);
        break_offset(env.printer, 1u, 4);

        // NOTE: In the AST implementation, the body of the function
        // block is stored in the "expr" field of the Block.
        assert!(b.stmts.is_empty());
        assert!(b.expr.is_some());

        // We extract the block, so as not to create another set of boxes
        match b.expr.unwrap().node {
            ExprBlock(ref blk) => {
                // This will close the ibox
                self.visit_block(blk, env);
            }
            _ => {
                // Close the ibox.
                end(env.printer);
                // This is a bare expression.
                // e.g., |x| foo(x)
                self.visit_expr(b.expr.unwrap(), env);
            }
        }
    }

    /// "fn" "(" [ [parameter ":" type], ... ] ")" -> type
    fn visit_fk_item_fn_decl(&mut self, function_declaration: &fn_decl, env: PpEnv) {
        word(env.printer, "(");
        // TODO: This should be in a box.
        cbox(env.printer, 0);
        join_with_comma(
            function_declaration.inputs,
            |arg| self.visit_arg(arg, env),
            env);
        end(env.printer);
        word(env.printer, ")");

        match function_declaration.output.node {
            ty_infer => {}
            ty_nil => {}
            _ => {
                env.nbsp();
                word(env.printer, "->");
                env.nbsp();
                self.visit_ty(&function_declaration.output, env);
            }
        }
    }

    // |x, y| -> type or
    // || -> type or
    // ||
    fn visit_fk_fn_block_decl(&mut self,
                              function_declaration: &fn_decl,
                              env: PpEnv) {
        word(env.printer, "|");
        join_with_comma(
            function_declaration.inputs,
            |arg| self.visit_arg(arg, env),
            env);
        word(env.printer, "|");

        match function_declaration.output.node {
            ty_infer => {}
            ty_nil => {}
            _ => {
                env.nbsp();
                word(env.printer, "->");
                env.nbsp();
                self.visit_ty(&function_declaration.output, env);
            }
        }
    }

    // proc(x, y) -> type or
    // proc() -> type or
    // proc()
    fn visit_fk_fn_block_proc_decl(&mut self,
                                   function_declaration: &fn_decl,
                                   env: PpEnv) {
        word(env.printer, "proc(");
        join_with_comma(
            function_declaration.inputs,
            |arg| self.visit_arg(arg, env),
            env);
        word(env.printer, ")");

        match function_declaration.output.node {
            ty_infer => {}
            ty_nil => {}
            _ => {
                env.nbsp();
                word(env.printer, "->");
                env.nbsp();
                self.visit_ty(&function_declaration.output, env);
            }
        }
    }


    // [if test {] block_contents }
    // TODO: if test block else else_expr
    fn visit_if(&mut self, test: @Expr, blk: &Block, opt_else: Option<@Expr>, env: PpEnv) {
        cbox(env.printer, 0);
        word(env.printer, "if");
        env.nbsp();
        self.visit_expr(test, env);
        break_offset(env.printer, 1u, 0);
        self.visit_block(blk, env);

        match opt_else {
            Some(_else) => {
                match _else.node {
                    // "final else"
                    ExprBlock(ref b) => {
                        env.nbsp();
                        // For the block to close
                        cbox(env.printer, 0);
                        word(env.printer, "else");
                        env.nbsp();
                        self.visit_block(b, env);
                    }
                    _ => {
                        env.nbsp();
                        word(env.printer, "else");
                        env.nbsp();
                        self.visit_expr(_else, env);
                    }
                }
            }
            _ => {}
        }
    }

    fn visit_path(&mut self, path: &Path, env: PpEnv) {
        for segment in path.segments.iter() {
            word(env.printer, ident_to_str(&segment.identifier));
            // for typ in segment.types.iter() {
            //     println!("typ: {:?}", typ);
            //     // self.visit_ty(typ, env)
            // }
        }
    }

    fn visit_literal(&mut self, lit: &lit, env: PpEnv) {
        // TODO: some other stuff from pprust

        match lit.node {
            // lit_str(st, style) => print_string(s, st, style),
            // lit_char(ch) => {
            //     let mut res = ~"'";
            //     char::from_u32(ch).unwrap().escape_default(|c| {
            //             res.push_char(c);
            //         });
            //     res.push_char('\'');
            //     word(env.printer, res);
            // }
            lit_int(i, t) => {
                if i < 0_i64 {
                    word(env.printer,
                         ~"-" + (-i as u64).to_str_radix(10u)
                         + ast_util::int_ty_to_str(t));
                } else {
                    word(env.printer,
                         (i as u64).to_str_radix(10u)
                         + ast_util::int_ty_to_str(t));
                }
            }
            // lit_uint(u, t) => {
            //     word(env.printer,
            //          u.to_str_radix(10u)
            //          + ast_util::uint_ty_to_str(t));
            // }
            lit_int_unsuffixed(i) => {
                if i < 0_i64 {
                    word(env.printer, ~"-" + (-i as u64).to_str_radix(10u));
                } else {
                    word(env.printer, (i as u64).to_str_radix(10u));
                }
            }
            // lit_float(f, t) => {
            //     word(env.printer, f.to_owned() + ast_util::float_ty_to_str(t));
            // }
            // lit_float_unsuffixed(f) => word(env.printer, f),
            // lit_nil => word(env.printer, "()"),
            // lit_bool(val) => {
            //     if val { word(env.printer, "true"); } else { word(env.printer, "false"); }
            // }
            // lit_binary(arr) => {
            //     ibox(s, indent_unit);
            //     word(env.printer, "[");
            //     commasep_cmnt(s, inconsistent, arr, |s, u| word(env.printer, format!("{}", *u)),
            //                   |_| lit.span);
            //     word(env.printer, "]");
            //     end(s);
            // }
            _ => {}
        }
    }

    // [parameter: type] or
    // [parameter]
    fn visit_arg(&mut self, arg: &arg, env: PpEnv) {
        cbox(env.printer, 4);
        match arg.ty.node {
            ty_infer => self.visit_pat(arg.pat, env),
            _ => {
                match arg.pat.node {
                    PatIdent(_, ref path, _) if
                        path.segments.len() == 1 &&
                        path.segments[0].identifier.name ==
                        parse::token::special_idents::invalid.name => {
                        // Do nothing.
                    }
                    _ => {
                        self.visit_pat(arg.pat, env);
                        word(env.printer, ":");
                        break_offset(env.printer, 1u, 0);
                    }
                }
                self.visit_ty(&arg.ty, env);
            }
        }
        end(env.printer);
    }

    /// [foo: BREAK val]
    fn visit_field(&mut self, field: &Field, env: PpEnv) {
        ibox(env.printer, INDENT_UNIT as uint);
        self.visit_ident(&field.ident.node, env);
        word(env.printer, ":");
        break_offset(env.printer, 1u, 0);
        self.visit_expr(field.expr, env);
        end(env.printer);
    }

    fn visit_ident(&mut self, ident: &Ident, env: PpEnv) {
        word(env.printer, ident_to_str(ident));
    }

    fn visit_expr_vstore(&mut self, t: &ExprVstore, env: PpEnv) {
        match *t {
            ExprVstoreUniq => word(env.printer, "~"),
            ExprVstoreBox => word(env.printer, "@"),
            ExprVstoreMutBox => {
                word(env.printer, "@");
                word(env.printer, "mut");
                env.nbsp();
            }
            ExprVstoreSlice => word(env.printer, "&"),
            ExprVstoreMutSlice => {
                word(env.printer, "&");
                word(env.printer, "mut");
                env.nbsp();
            }
        }
    }

    /// "fn" "(" [ [parameter ":" type], ... ] ")" -> type
    fn visit_fk_item_fn_decl(&mut self, function_declaration: &fn_decl, env: PpEnv) {
        word(env.printer, "(");
        // TODO: This should be in a box.
        cbox(env.printer, 0);
        join_with_comma(
            function_declaration.inputs,
            |arg| self.visit_arg(arg, env),
            env);
        end(env.printer);
        word(env.printer, ")");

        match function_declaration.output.node {
            ty_infer => {}
            ty_nil => {}
            _ => {
                env.nbsp();
                word(env.printer, "->");
                env.nbsp();
                self.visit_ty(&function_declaration.output, env);
            }
        }
    }

    // |x, y| -> type or
    // || -> type or
    // ||
    fn visit_fk_fn_block_decl(&mut self,
                              function_declaration: &fn_decl,
                              env: PpEnv) {
        word(env.printer, "|");
        join_with_comma(
            function_declaration.inputs,
            |arg| self.visit_arg(arg, env),
            env);
        word(env.printer, "|");

        match function_declaration.output.node {
            ty_infer => {}
            ty_nil => {}
            _ => {
                env.nbsp();
                word(env.printer, "->");
                env.nbsp();
                self.visit_ty(&function_declaration.output, env);
            }
        }
    }

    // [if test {] block_contents }
    // TODO: if test block else else_expr
    fn visit_if(&mut self, test: @Expr, blk: &Block, opt_else: Option<@Expr>, env: PpEnv) {
        cbox(env.printer, 0);
        word(env.printer, "if");
        env.nbsp();
        self.visit_expr(test, env);
        break_offset(env.printer, 1u, 0);
        self.visit_block(blk, env);

        match opt_else {
            Some(_else) => {
                match _else.node {
                    // "final else"
                    ExprBlock(ref b) => {
                        env.nbsp();
                        // For the block to close
                        cbox(env.printer, 0);
                        word(env.printer, "else");
                        env.nbsp();
                        self.visit_block(b, env);
                    }
                    _ => {
                        env.nbsp();
                        word(env.printer, "else");
                        env.nbsp();
                        self.visit_expr(_else, env);
                    }
                }
            }
            _ => {}
        }
    }

    fn visit_path(&mut self, path: &Path, env: PpEnv) {
        for segment in path.segments.iter() {
            word(env.printer, ident_to_str(&segment.identifier));
            // for typ in segment.types.iter() {
            //     println!("typ: {:?}", typ);
            //     // self.visit_ty(typ, env)
            // }
        }
    }

    fn visit_literal(&mut self, lit: &lit, env: PpEnv) {
        // TODO: some other stuff from pprust

        match lit.node {
            // lit_str(st, style) => print_string(s, st, style),
            // lit_char(ch) => {
            //     let mut res = ~"'";
            //     char::from_u32(ch).unwrap().escape_default(|c| {
            //             res.push_char(c);
            //         });
            //     res.push_char('\'');
            //     word(env.printer, res);
            // }
            lit_int(i, t) => {
                if i < 0_i64 {
                    word(env.printer,
                         ~"-" + (-i as u64).to_str_radix(10u)
                         + ast_util::int_ty_to_str(t));
                } else {
                    word(env.printer,
                         (i as u64).to_str_radix(10u)
                         + ast_util::int_ty_to_str(t));
                }
            }
            // lit_uint(u, t) => {
            //     word(env.printer,
            //          u.to_str_radix(10u)
            //          + ast_util::uint_ty_to_str(t));
            // }
            lit_int_unsuffixed(i) => {
                if i < 0_i64 {
                    word(env.printer, ~"-" + (-i as u64).to_str_radix(10u));
                } else {
                    word(env.printer, (i as u64).to_str_radix(10u));
                }
            }
            // lit_float(f, t) => {
            //     word(env.printer, f.to_owned() + ast_util::float_ty_to_str(t));
            // }
            // lit_float_unsuffixed(f) => word(env.printer, f),
            // lit_nil => word(env.printer, "()"),
            // lit_bool(val) => {
            //     if val { word(env.printer, "true"); } else { word(env.printer, "false"); }
            // }
            // lit_binary(arr) => {
            //     ibox(s, indent_unit);
            //     word(env.printer, "[");
            //     commasep_cmnt(s, inconsistent, arr, |s, u| word(env.printer, format!("{}", *u)),
            //                   |_| lit.span);
            //     word(env.printer, "]");
            //     end(s);
            // }
            _ => {}
        }
    }

    // [parameter: type] or
    // [parameter]
    fn visit_arg(&mut self, arg: &arg, env: PpEnv) {
        cbox(env.printer, 4);
        match arg.ty.node {
            ty_infer => self.visit_pat(arg.pat, env),
            _ => {
                match arg.pat.node {
                    PatIdent(_, ref path, _) if
                        path.segments.len() == 1 &&
                        path.segments[0].identifier.name ==
                        parse::token::special_idents::invalid.name => {
                        // Do nothing.
                    }
                    _ => {
                        self.visit_pat(arg.pat, env);
                        word(env.printer, ":");
                        break_offset(env.printer, 1u, 0);
                    }
                }
                self.visit_ty(&arg.ty, env);
            }
        }
        end(env.printer);
    }

    /// [foo: BREAK val]
    fn visit_field(&mut self, field: &Field, env: PpEnv) {
        ibox(env.printer, INDENT_UNIT as uint);
        self.visit_ident(&field.ident.node, env);
        word(env.printer, ":");
        break_offset(env.printer, 1u, 0);
        self.visit_expr(field.expr, env);
        end(env.printer);
    }

    fn visit_ident(&mut self, ident: &Ident, env: PpEnv) {
        word(env.printer, ident_to_str(ident));
    }

    fn visit_expr_vstore(&mut self, t: &ExprVstore, env: PpEnv) {
        match *t {
            ExprVstoreUniq => word(env.printer, "~"),
            ExprVstoreBox => word(env.printer, "@"),
            ExprVstoreMutBox => {
                word(env.printer, "@");
                word(env.printer, "mut");
                env.nbsp();
            }
            ExprVstoreSlice => word(env.printer, "&"),
            ExprVstoreMutSlice => {
                word(env.printer, "&");
                word(env.printer, "mut");
                env.nbsp();
            }
        }
    }
}

// TODO: This code duplicates code in visit_arm. Refactor.
fn is_simple_expr_body(body: &Block) -> bool {
    if body.view_items.is_empty() &&
        body.stmts.is_empty() &&
        body.rules == DefaultBlock &&
        body.expr.is_some() {
        match body.expr {
            Some(expr) => {
                match expr.node {
                    ExprBlock(ref blk) => {
                        false
                    }
                    _ => {
                        true
                    }
                }
            }
            _ => false
        }
    } else {
        false
    }
}

/// Iterate over list calling elem_fn on each element and separator_fn in
/// between elements.
///
/// The two functions are interesting mainly for their side-effects.
pub fn join_fns_with_sep<A>(list: &[A], elem_fn: &fn(&A), separator_fn: &fn()) {
    if list.is_empty() {
        return;
    }
    for elem in list.iter().take(list.len() - 1) {
        elem_fn(elem);
        separator_fn();
    }
    elem_fn(&list[list.len() - 1]);
}

// Join elem_fn() of each element with "," BREAK.
pub fn join_with_comma<A>(list: &[A], elem_fn: &fn(&A), env: PpEnv) {
    join_fns_with_sep(
        list,
        elem_fn,
        || {
            word(env.printer, ",");
            break_offset(env.printer, 1u, 0);
        });
}

#[test]
fn test_join_fns_with_sep(){
    let foo = ~[1, 2, 3];
    let mut bar: ~[int] = ~[];
    join_fns_with_sep(foo,
                      |x| bar.push(*x),
                      || bar.push(999));
    assert_eq!(bar, ~[1, 999, 2, 999, 3]);

    let mut baz = 0;
    join_fns_with_sep(~[1, 2, 3],
                      |x| baz += *x,
                      || if baz == 3 { baz = 0; });
    assert_eq!(baz, 3);
}

pub fn pretty_print(file_name: &str) {
    let path = std::path::Path::new(file_name);
    let on_error = || fail!("open of {:?} failed", path);
    let mut reader: File = File::open(&path).unwrap_or_else(on_error);
    let source_str = str::from_utf8_owned(reader.read_to_end());

    let crate = string_to_crate(source_str.to_managed());
    let mut visitor = PpVisitor;
    let mut env = PpEnv::new();

    walk_crate(&mut visitor, &crate, env);
    // TODO: Put this at the end of the "visit_crate" function
    eof(env.printer);

    let actual = str::from_utf8(*env.output.inner_ref());
    println!("{:s}", actual);
}

fn print_usage(program: &str, _opts: &[Opt]) {
    println!("Usage: {} input-file [options]", program);
    println("-h --help\tUsage");
}

fn main() {
    let args = os::args();

    let program = args[0].clone();

    let opts = ~[
        optflag("h"),
        optflag("help")
            ];
    let matches = match getopts(args.tail(), opts) {
        Ok(m) => { m }
        Err(f) => { fail!(f.to_err_msg()) }
    };
    if matches.opt_present("h") || matches.opt_present("help") {
        print_usage(program, opts);
        return;
    }
    let input_file: &str = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        print_usage(program, opts);
        return;
    };

    pretty_print(input_file);
}
