#!/bin/bash

# Compare each .rs file in $test_dir against the corresponding .pp file.

# TODO: Maybe reuse the same output file instead of creating and deleting a
# file in each iteration

# TODO: Colorize the output (Green for tests that succeed, etc.)

test_dir=./test

num_tests=0
num_succ=0
num_fail=0
num_skipped=0

for rs_file in $test_dir/*.rs; do
    num_test=$(($num_test + 1))

    expected=$test_dir/$(basename $rs_file .rs).pp

    if [ ! -r $expected ]; then
	num_skipped=$(($num_skipped + 1))
	echo "Test: $rs_file SKIPPED - corresponding pp file not readable or does not exist."
	continue;
    fi

    RUST_LOG=my_pp,pp ./my_pp $rs_file > /tmp/$$actual

    cmp /tmp/$$actual $expected &> /dev/null

    if [ $? -eq 0 ]
    then
	echo "Test: $rs_file SUCCESS"
	num_succ=$(($num_succ + 1))
    else
	num_fail=$(($num_fail + 1))
	echo "Test: $rs_file FAIL"

	# -c is for context format, -u is for unified (compressed) format
	# diff -c -L expected -L actual $expected /tmp/$$actual | head
	diff -u -L expected -L actual $expected /tmp/$$actual

	# echo "----------------"
	# echo "Expected: "
	# # cat $expected
	# head $expected
	# echo "----------------"
	# echo "Actual: "
	# # cat /tmp/$$actual
	# head /tmp/$$actual
	# echo "----------------"
    fi
    rm /tmp/$$actual
done

# TODO: Print "test result: FAILED" (or SUCCESS)
echo "Summary:" $num_test "tests;" $num_succ "succeeded;" $num_fail "failed." $num_skipped "skipped."

if [ $num_fail -gt 0 ]; then
    exit 101
fi
