fn main() {
    let some_long_name_foo_foo_foo = (1,);
    let some_long_name_foo_foo_foo = (1, 2, 3, 4);
    let some_long_name_foo_foo_foo = (33333333,
                                      33333333,
                                      33333333,
                                      33333333,
                                      33333333,
                                      33333333,
                                      33333333,
                                      33333333);
}
