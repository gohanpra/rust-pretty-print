fn main() {
    let some_long_name_foo_foo_foo = ~[1, 2, 3, 4];
    let some_long_name_foo_foo_foo = @mut [1, 2, 3, 4];
    let some_long_name_foo_foo_foo = @mut [Foo {
                                               x_foo_foo_foo: 3,
                                               y_foo_foo_foo: 5,
                                           }];
}
