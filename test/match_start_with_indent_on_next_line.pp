fn some_fn_with_a_very_long_name_to_force_it_not_to_be_a_one_liner() {
    let x = 1;
    match x {
        Some(y) =>
            some_fn_with_a_very_long_name_to_force_it_not_to_be_a_one_liner(),
        None =>
            some_fn_with_a_very_long_name_to_force_it_not_to_be_a_one_liner()
    }
}
