#[feature(managed_boxes)];
extern mod syntax;
use syntax::print::pp::{break_offset, word, space, zerobreak, hardbreak};
use syntax::print::pp::{breaks, consistent, inconsistent, eof};
use syntax::print::pp;

use std::char;
use std::str;
use std::io;
use std::io::Decorator;
use std::io::mem::MemReader;
use std::io::mem::MemWriter;

fn main(){
    let writer = @mut MemWriter::new();
    let printer = pp::mk_printer(writer as @mut io::Writer, 20u);

    pp::cbox(printer, 0);

    pp::ibox(printer, 0);
    pp::word(printer, &"fn");
    break_offset(printer, 1u, 0);
    pp::word(printer, &"foo_foo(x: int)");
    break_offset(printer, 1u, 0);
    pp::word(printer, &"{");
    pp::end(printer);

    // pp::break_offset(printer, 0u, 4);

    // pp::cbox(printer, 0);
    // pp::break_offset(printer, 1u, 0);
    // pp::word(printer, &"x");
    // // pp::word(printer, &"let x = 4;");
    // pp::break_offset(printer, 1u, 0);
    // pp::end(printer);

    // pp::break_offset(printer, 0u, 0);

    pp::word(printer, &"}");

    pp::end(printer);

    pp::eof(printer);
    let output_str = str::from_utf8(*writer.inner_ref());
    println!("output_str: \n{:s}", output_str);
}

struct Tree {
    name: ~str,
    children: ~[Tree],
}

impl Tree {
    pub fn new(name: ~str, children: ~[Tree]) -> Tree {
        Tree {name: name, children: children}
    }

    fn pretty_print_v1(&self, printer: @mut pp::Printer) {
        pp::cbox(printer, self.name.len() + 1);
        pp::word(printer, self.name.as_slice());
        if self.children.len() != 0 {
            pp::word(printer, &"[");
            for tree in self.children.iter().take(self.children.len() - 1) {
                tree.pretty_print_v1(printer);
                pp::word(printer, &",");
                pp::space(printer);
            }
            self.children[self.children.len() - 1].pretty_print_v1(printer);
            pp::word(printer, &"]");
        }
        pp::end(printer);
    }

    fn pretty_print_v2(&self, printer: @mut pp::Printer) {
        pp::cbox(printer, self.name.len() + 1);
        pp::word(printer, self.name.as_slice());
        if self.children.len() != 0 {
            pp::word(printer, &"[");
            for tree in self.children.iter().take(self.children.len() - 1) {
                tree.pretty_print_v1(printer);
                pp::word(printer, &",");
                pp::space(printer);
            }
            self.children[self.children.len() - 1].pretty_print_v1(printer);
            pp::word(printer, &"]");
        }
        pp::end(printer);
    }

    fn to_str_v2(&self) -> ~str {
        let writer = @mut MemWriter::new();
        let printer = pp::mk_printer(writer as @mut io::Writer, 15u);

        self.pretty_print_v2(printer);

        pp::eof(printer);

        let output_str = str::from_utf8(*writer.inner_ref());
        output_str
    }
}

impl ToStr for Tree {
    fn to_str(&self) -> ~str {

        let writer = @mut MemWriter::new();
        let printer = pp::mk_printer(writer as @mut io::Writer, 15u);

        self.pretty_print_v1(printer);

        pp::eof(printer);

        let output_str = str::from_utf8(*writer.inner_ref());
        output_str
    }
}

#[ignore]
#[test]
fn test_consistent_breaking_width_15(){
    let c_tree = Tree::new(~"ccc", ~[]);
    let d_tree = Tree::new(~"dd", ~[]);
    let b_tree = Tree::new(~"bbbb", ~[c_tree, d_tree]);

    let e_tree = Tree::new(~"eee", ~[]);

    let g_tree = Tree::new(~"gg", ~[]);
    let h_tree = Tree::new(~"hhh", ~[]);
    let i_tree = Tree::new(~"ii", ~[]);

    let f_tree = Tree::new(~"ffff", ~[g_tree, h_tree, i_tree]);

    let a_tree = Tree::new(~"aaa", ~[b_tree, e_tree, f_tree]);

    let expected = ~"aaa[bbbb[ccc, dd], eee, ffff[gg, hhh, ii]]";
    let actual = a_tree.to_str();

    assert!(actual == expected,
            format!("\nactual:\n{}\nexpected:\n{}\n", actual, expected));
}

#[test]
fn test_consistent_breaking_width_15_two_BEGs(){
    let c_tree = Tree::new(~"ccc", ~[]);
    let d_tree = Tree::new(~"dd", ~[]);
    let b_tree = Tree::new(~"bbbb", ~[c_tree, d_tree]);

    let e_tree = Tree::new(~"eee", ~[]);

    let g_tree = Tree::new(~"gg", ~[]);
    let h_tree = Tree::new(~"hhh", ~[]);
    let i_tree = Tree::new(~"ii", ~[]);

    let f_tree = Tree::new(~"ffff", ~[g_tree, h_tree, i_tree]);

    let a_tree = Tree::new(~"aaa", ~[b_tree, e_tree, f_tree]);

    let expected = ~"aaa[bbbb[ccc, dd], eee, ffff[gg, hhh, ii]]";
    let actual = a_tree.to_str_v2();

    assert!(actual == expected,
            format!("\nactual:\n{}\nexpected:\n{}\n", actual, expected));
}

#[ignore]
#[test]
fn test_consistent_breaking_width_15_one_char(){
    let c_tree = Tree::new(~"c", ~[]);
    let d_tree = Tree::new(~"d", ~[]);
    let b_tree = Tree::new(~"b", ~[c_tree, d_tree]);

    let e_tree = Tree::new(~"e", ~[]);

    let g_tree = Tree::new(~"g", ~[]);
    let h_tree = Tree::new(~"h", ~[]);
    let i_tree = Tree::new(~"i", ~[]);

    let f_tree = Tree::new(~"f", ~[g_tree, h_tree, i_tree]);

    let a_tree = Tree::new(~"a", ~[b_tree, e_tree, f_tree]);

    let expected = ~"a[b[c, d], e, f[g, h, i]]";
    let actual = a_tree.to_str();

    assert!(actual == expected,
            format!("\nactual:\n{}\nexpected:\n{}\n", actual, expected));
}
